const fs = require('fs');
const path = require('path');

// // reference to that data folder
// const p = path.join(path.dirname(require.main.filename), 'data','product.json');


// // create a function thats going to read the file 
// const getProductsFromFile = cb => {
//     // read file returns the content within that file 
//     // its going with a call back file if there is a error 
//     // we can check for that but if there data in there 
//     // the file content is going to be with in that file
//     fs.readFile(p, (err, fileContent) => {
//         if(err){
//             cb([])
//         }else {
//             // its going to be in json format
//             // is going to return a javascript object
//             cb(JSON.parse(fileContent));
//         }
//     })
// }