// send files through the server

const path = require('path');

const express = require('express');


const rootDir = require('../utils/path');
// we are going to create a endpoint
const router = express.Router();


router.get('/', (req, res) => {
    // res.sendFile(path.join(rootDir, 'views', 'shop.html'));
    // when we send to template engine we render files
    //app.set('views', 'views') were going to render the file
    // ejs you can render different pages 
    res.render('shop/index', {
        // you can create a object and create data 
        // and reference them in here.
        pageTitle: 'Shop',
        path: '/'
    });
});

module.exports = router;
