const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');


const app = express();

const shopRoutes = require('./routes/shop');
const adminRoutes = require('./routes/admin');
// to get chunks of code. gets the data
app.use(bodyParser.urlencoded({extended: false}));

app.set('view engine', 'ejs');

app.set('views', 'views')
// middleware - before the excution of the express listen app
// were going to let express know about the routes and 
// which body parser to use

// gets the css file or recognize the public file 
app.use(express.static(path.join(__dirname, 'public')));


// middleware
// sets the application setting to val, or return setting's value.

app.use('/admin', adminRoutes); // endpoint: 'admin/add-product'
app.use(shopRoutes); // endpoint: '/'



app.listen(4000);



