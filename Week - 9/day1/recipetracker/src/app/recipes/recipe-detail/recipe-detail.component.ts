import {
  Component,
  OnInit
} from '@angular/core';
import {
  ActivatedRoute,
  Router
} from '@angular/router';
import {
  map,
  switchMap
} from 'rxjs/operators';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
  id: number;

  recipe = {
    name: 'Steak',
    description: 'juicy',
    imagePath: 'https://i2.wp.com/www.foodrepublic.com/wp-content/uploads/2012/05/testkitchen_argentinesteak.jpg?resize=1280%2C%20560&ssl=1',
    ingredients: [{
        name: 'steak',
        amount: 1
      },
      {
        name: 'butter',
        amount: 1
      },
      {
        name: 'salt',
        amount: 1
      },
      {
        name: 'pepper',
        amount: 1
      },
      {
        name: 'basil',
        amount: 1
      },
    ]
  }

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {

  }

  onAddToShoppingList() {
    // dispatch add ingredients..
  }

  onEditRecipe() {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }

  onDeleteRecipe() {
    // dispatch action to delete recipe..
    this.router.navigate(['/recipes']);
  }

}
