import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

// create model for recipe.. need to set up database first.

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit, OnDestroy {

  constructor(private router: Router, private route: ActivatedRoute) { }

  recipes = [
    {
      name: 'Steak',
      description: 'juicy',
      imagePath: 'https://i2.wp.com/www.foodrepublic.com/wp-content/uploads/2012/05/testkitchen_argentinesteak.jpg?resize=1280%2C%20560&ssl=1',
      ingredients: [{
          name: 'steak',
          amount: 1
        },
        {
          name: 'butter',
          amount: 1
        },
        {
          name: 'salt',
          amount: 1
        },
        {
          name: 'pepper',
          amount: 1
        },
        {
          name: 'basil',
          amount: 1
        },
      ]
    }
  ]

  ngOnInit(): void {
    // subscription to get data from store / database..
  }

  onNewRecipe() {
    this.router.navigate(['new'], { relativeTo: this.route })
  }

  ngOnDestroy() {

  }

}
