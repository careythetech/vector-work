const path = require('path')
const mongoose = require('mongoose');
const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
require('dotenv').config()
// const csrf = require('csurf');
// const flash = require('connect-flash');
const { notFound, errorHandler } = require('./middleware/errorMiddleware')

const app = express();
const userRoutes = require('./routes/users');
const authRoutes = require('./routes/auth');
// const authRoutes = require('./routes/auth');

// const csrfProtection = csrf();
// routes
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json());
// app.use(
//         session({
//                 secret: 'my secret',
//                 resave: false,
//                 saveUninitialized: false,
//         })
// );
// app.use(csrfProtection);
// app.use(flash());
// app.use((req, res, next) => {
//         res.locals.isAuthenticated = req.session.isLoggedIn;
//         res.locals.csrfToken = req.csrfToken();
//         next();
// });

// app.use("/users", userRoutes);
app.use("/auth", authRoutes);
app.use(notFound);
app.use(errorHandler);

const MONGODB_URI = "mongodb+srv://adminuser123:adminuser123100@cluster0.9jqmk.mongodb.net/?retryWrites=true&w=majority";

mongoose.connect(MONGODB_URI).then(() => {
        app.listen(8080);
        console.log('connected to mongoose');
}).catch(err => console.log(err))