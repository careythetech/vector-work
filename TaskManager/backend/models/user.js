const  mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const { Schema } = mongoose

const userSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    isAdmin: {
        type: Boolean,
        required: true,
        default: false
    },
    picture: {
        type: String,
        required: true,
        default: 'https://cdn.shopify.com/s/files/1/0573/6631/5108/files/Screen_Shot_2022-05-17_at_10.26.35_PM_250x@2x.png?v=1653525786'
    },
}, ({timestamps: true}));

userSchema.pre('save', async function(next) {
    if(!this.isModified('password')) {
        next();
    }
    const salt = await bcrypt.genSalt(10);
    this.password = await bcrypt.hash(this.password, salt)
})

userSchema.methods.matchPassword = async function(enterPassword) {
            return await bcrypt.compare(enterPassword, this.password);
}
module.exports = mongoose.model('User', userSchema);