const User = require('../models/User');
const asyncHandler = require('express-async-handler');
const generateToken  = require('../generateToken')

const registerUser = asyncHandler(async (req,res) => {
    const { name, email, password, picture, isAdmin } = req.body;
    const user = await User.create({name: name, email: email, password: password, picture: picture, isAdmin: isAdmin})
    if(user){
        res.json({
            _id: user._id,
            name: user.name,
            email: user.email,
            isAdmin: user.isAdmin,
            password: user.password,
            picture: user.picture,
            })
            res.status(200).send({message: 'User created successfully', token: generateToken(user._id),})
    } else {
        res.status(404).send({message: 'Error Occured Creating User'})
    }
    const foundUser = await User.findOne({email});
    if(foundUser){
        res.status(400).send({message: 'User already Exist'})
    }
});

    
const authUser = asyncHandler(async (req, res) => {
    const { email, password, } = req.body;
    const user = await User.findOne({ email });
    if(user && (await user.matchPassword(password))) {
        res.status(200).send({message: 'User logged In successfully', token: generateToken(user._id),})
        res.json({
            _id: user._id,
            name: user.name,
            email: user.email,
            isAdmin: user.isAdmin,
            picture: user.picture,
        })
    }else {
        res.status(404).send({message: 'Error Occured login'})
    }
})

module.exports = { registerUser, authUser };