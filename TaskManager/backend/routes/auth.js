const express = require('express');
const { registerUser, authUser }= require('../controller/authController')
const router = express.Router();

router.route('/register').post(registerUser);
router.route('/login').get(authUser);

module.exports = router;