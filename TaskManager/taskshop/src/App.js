import React from 'react';
import { BrowserRouter, Route, Routes} from "react-router-dom";
import LandingPage from "../src/components/LandingPage";
import LoginPage from "./components/LoginPage";
import RegisterPage from "../src/components/RegisterPage";
import Header from "../src/components/Header";
import Footer from "../src/components/Footer";

function App() {
  return (
    <BrowserRouter>
    <Header/>
    <Routes>
    <Route path="/" component={LandingPage} exact/>
      <Route path="/login" component={LoginPage} exact/>
      <Route path="/register" component={RegisterPage} exact/>
    </Routes>
      <Footer/>
    </BrowserRouter>
  );
}

export default App;
