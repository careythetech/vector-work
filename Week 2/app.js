// // The Old Way for request.
// (function () {
//     const endPoint = "https://www.thecocktaildb.com/api/json/v1/1/search.php?s=margarita";
//     const getBtnDrink = document.getElementById('btnDrink');
//     const drinkText = document.getElementById('drink');
//     function getRandomDrink() {
//         const request = new XMLHttpRequest();
//         request.onreadystatechange = function () {
//             if (request.readyState !== 4) {
//                 return;
//             }
//             if (request.status >= 200 && request.status < 300) {
//                 // console.log(JSON.parse(request.responseText));
//                 const { strDrink } = JSON.parse(request.responseText);
//                 return strDrink;
//                 // gets random drink 
//             }
//         };
//         request.open('GET', endPoint);
//         request.send();
//     };
//     getBtnDrink.addEventListener('click', function() {
//                 getRandomDrink(function(strDrink){
//                     drinkText.innerText = strDrink; 
//         });
        
//         // const drinkFound = getRandomDrink();
//         // drinkText.innerText = "Drink";
//     });
// })();



// if you want to write something asyncornis you want to return a promise
(function () {
    // const endPoint = "https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=Non_Alcoholic";
    // const getBtnDrink = document.getElementById('btnDrink');
    // const drinkText = document.getElementById('drink');

    // function promiseRequestRandomDrink() {
    //     const request = new XMLHttpRequest();
    //     return new Promise((resolve, reject) => {
    //         request.onreadystatechange = function () {
    //             if (request.readyState !== 4) {
    //                 return;
    //             }
    //             if (request.status >= 200 && request.status < 300) {
    //                 console.log(JSON.parse(request.responseText));
    //                 const { strDrink } = JSON.parse(request.responseText).drink[0];
    //                 resolve(strDrink);
    //             }
    //         };
    //         request.open('GET', endPoint);
    //         request.send();
        // });

        // };
    // a fetch returns a promise
    // function fetchData(url){
    //     return fetch(url);
    // }
    // getBtnDrink.addEventListener('click', async function() {
    //     // // .then gets the data it promise it.
    //     // promiseRequestRandomDrink().then(data => {
    //     //     drinkText.innerText = data;
    //     // })
        
    //     // fetchData(endPoint).then(data => data.json()).then(offDrink =>{
    //     //     console.log(offDrink);
    //     //     drinkText.innerText = offDrink.drinks[3].strDrink;
    //     // });
// promise
    //     const response = await fetchData(endPoint);
    //     const data = await response.json();
    //     drinkText.innerText = data.drinks[4].strDrink;
    // });

    // arrow function 

    // const arrowTest = {
    //     a: 100,
    //     b: 200,
    //     c: function(){
    //         // return () => {
    //         //     return this.a + this.b;
    //         // }

    //         // return () => this.a + this.b;

    //         return a => this.a + this.b;  
    //     }
    // }
    // console.log(arrowTest.c()())


    // function construtor 
    // function Vehicle(make , model, year, color){
    //     this.make = make;
    //     this.model = model;
    //     this.year = year;
    //     this.color = color;
    // };

    // const telsa = new Vehicle('Telsa', 's', 2020, 'red');
    // console.log(telsa);
    // const {make , model, year, color} = telsa
    // console.log(make , model, year, color)


    // function genrateAmbioDataHTMl(){
    //     return `

    //     ` 
    // }
})();
