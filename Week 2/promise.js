(function () {
    const allBtn = document.getElementById('all')
    const allSettledBtn = document.getElementById('allSettled')
    const raceBtn = document.getElementById('race')


    allBtn.addEventListener('click', all);
    allSettledBtn.addEventListener('click', allSettled);
    raceBtn.addEventListener('click', race);


    const walmartEndpoint = 'url'
    const bestBuyEndpoint = 'url'
    const frysEndpoint = 'url'

    function getWalmartProducts() {
        return fetch(walmartEndpoint).then(data => data.json());
    }
    function getBestBuyProducts() {
        return fetch(bestBuyEndpoint).then(data => data.json());
    }
    function getfrysProducts() {
        return fetch(frysEndpoint).then(data => data.json());
    }


    function all() {
        // this will test all the data if its successfull
        let htmlData = '';
        Promise.all([getBestBuyProducts(), getWalmartProducts(), getfrysProducts()]).then(data => {
            console.log('check data..')
            console.log(data);
            data.forEach(product => {
                const {description, imgUrl, name, price} = product;
                htmlData += genrateCard(description, imgUrl, name, price);
            })
            products.innerHTML = htmlData
        }).catch(err => console.log('opps something is wrong', err))
    }

    function allSettled() {
        let htmlData = " ";
        Promise.allSettled([getBestBuyProducts(), getWalmartProducts(), getfrysProducts()]).then(results => {
            results.forEach(storeData => {
                if(storeData.value && storeData.status === 'fullfilled'){
                        storeData.value.forEach(product => {
                            const { description, imgUrl, name, price } = product
                            htmlData += genrateCard(description, imgUrl, name, price)
                        });
                        products.innerHTML = htmlData;
                }
            })
        }).catch(err => console.log('opps something is wrong', err))
    }

    function race() {
        let htmlData = " "
        Promise.race([getBestBuyProducts(), getWalmartProducts(), getfrysProducts()]).then(results => {
            results.forEach(product => {
                const { description, imgUrl, name, price } = product
                htmlData += genrateCard(description, imgUrl, name, price)
            })
            products.innerHTML = htmlData
        })
    }

    function genrateCard(description, imgUrl, name, price) {
        return `
        <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="{{ imgUrl}}" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <p class="card-text">${description}</p>
                <a href="#" class="btn btn-primary">${price}</a>
            </div>
        </div>`
    }
})();