(function () {
    const nameInput = document.getElementById('name').value; // grab the value in the current state so when you open page extract value when button is clicked.
    const taskInput = document.getElementById('task').value;
    const create = document.getElementById('create');
    const update = document.getElementById('update');
    const deleteBtn = document.getElementById('delete');

    const todoEndpoint = 'https://todo-app-e4b46-default-rtdb.firebaseio.com/.json';
    const table = document.getElementById('tasksTable');

    function uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    console.log(uuidv4());




    // Get DATA
    async function initApp() {
        const response = await fetch(todoEndpoint);
        const results = await response.json();
        let tableHtml = '';


        // To loop through Object we use for in
        // for(const key in results) {
        //     if(results.hasOwnProperty(key)) {
        //         const { id, name, task, created } = results[key];
        //         tableHtml += genrateTaskHTML(id, name, task, created);
        //     }
        // }

        Object.keys(results).forEach(key => {
            const { id, name, task, created } = results[key];
            tableHtml += genrateTaskHTML(id, name, task, created);
            //    console.log(key); // testing
        })
        table.innerHTML = tableHtml



        // This is how you loop through a Array
        // results.forEach(todo => {
        //     const {
        //         id,
        //         name,
        //         task,
        //         created
        //     } = todo;
        //     // it add in sign 
        //     tableHtml += genrateTaskHTML(id, name, task, created);
        //     // console.log(task)
        // });
        //------------
        // table.innerHTML = tableHtml
        //     // console.log(results)
    }

    // Create New Data
    async function createTask() {
        const nameVal = nameInput.value;
        const taskVal = taskInput.value;
        const userId = uuidv4();
        const createdVal = Date.now();

        const body = JSON.stringify({
            id: userId,
            name: nameVal,
            task: taskVal,
            created: createdVal
        });

        const response = await fetch(`https://todo-app-e4b46-default-rtdb.firebaseio.com/${userId}/.json`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body
        });
        const results = await response.json();

        // const { id, name, task, created } = results;

        // table.innerHTML += genrateTaskHTML(id, name, task, created);
        // console.log(id, name, task, created)
        // console.log(results);
    }


    // update task target specfic id 
    async function updateTask() {
        const nameVal = nameInput.value;
        const taskVal = taskInput.value;
        const body = JSON.stringify({
            name: nameVal,
            task: taskVal
        });

        const response =  await fetch(`https://todo-app-e4b46-default-rtdb.firebaseio.com/dasdsddsA/.json`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body
        });
        const result = await response.json();
        console.log(result)
        // console.log(id, name, task, created)
        // console.log(results);

    }

    // delete 
    async function deleteTask() {
        // grab the id
        const deleteUrl = `https://todo-app-e4b46-default-rtdb.firebaseio.com/74cbd62e-bf2b-497b-971a-d2e152dbcf4c/.json`;
        const response = await fetch(deleteUrl, {
            method: 'DELETE'
        });
    }


    function genrateTaskHTML(id, task, name, created) {
        return `
        <tr id ="${id}">
        <th scope="row">${id}</th>
        <td>${task}</td>
        <td>${name}</td>
        <td>${created}</td>
        </tr>
        `;
    }

    // create.addEventListener('click', () => {
    //     const nameVal = name.value;
    //     const taskVal = task.value;

    // });

    //create task event listner
    create.addEventListener('click', createTask)
    update.addEventListener('click', updateTask)
    deleteBtn.addEventListener('click', deleteTask)

    initApp()
})();