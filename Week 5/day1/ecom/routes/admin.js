const express = require('express');

const isAuth = require('../middleware/is-auth');

const adminController = require('../controllers/admin');

const { body } = require('express-validator/check');

const router = express.Router();

router.get('/add-product', isAuth, adminController.getAddProduct);

router.post('/add-product', [
    body('title')
        .isString()
        .isLength({ min: 3 }),
        isAuth,
], adminController.postAddProduct);

router.get('/products', isAuth, adminController.getProducts);

router.get('/productslocal', adminController.getProductsLocal);

router.get('/edit-product/:productId', isAuth, adminController.getEditProduct);

router.post('/edit-product', isAuth, adminController.postEditProduct);

router.post('/delete-product', isAuth, adminController.postDeleteProduct);

module.exports = router;