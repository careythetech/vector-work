const Product = require('../models/product');

exports.getProducts = (req, res) => {
    Product.find().then(products => {
        res.render('shop/product-list', {
            prods: products,
            pageTitle: 'All Products',
            path: '/products'
        })
    })
};

exports.getProduct = (req, res) => {
    const prodId = req.params.productId;
    Product.findById(prodId).then(product => {
        res.render('shop/product-detail', {
            product,
            pageTitle: product.title,
            path: '/products'
        })
    }).catch(err => console.log(err));
}

exports.getIndex = (req, res) => {
    Product.find().then(products => {
        res.render('shop/index', {
            prods: products,
            pageTitle: 'Shop',
            path: '/'
        })
    }).catch(err => console.log(err));
}

exports.getCart = (req, res) => {
    req.user.populate('cart.items.productId').execPopulate().then(user => {
        const products = user.cart.items;
        console.log('check products');
        console.log(products);
        res.render('shop/cart', {
            path: '/cart',
            pageTitle: 'Your Cart',
            products
        })
    })
}

exports.postCart = (req, res) => {
    const prodId = req.body.productId;
    Product.findById(prodId).then(product => {
        return req.user.addToCart(product);
    }).then(() => {
        res.redirect('/cart');
    }).catch(err => console.log(err));
}

exports.postCartDeleteProduct = (req, res) => {
    const { productId } = req.body;
    console.log('check productId..');
    console.log(productId);
    req.user.removeFromCart(productId).then(() => {
        res.redirect('/cart');
    }).catch(err => console.log(err));
}