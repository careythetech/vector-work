// Testing first render example..
// class Test extends React.Component {
//     render() {
//         return (
//             <h1>Hello from React!!!??!!!</h1>
//         )
//     }
// };


// class Toggle extends React.Component {
//     constructor() {
//         super();
//         this.toggle = this.toggle.bind(this);
//         /*
//             Virtual DOM
//             virtual DOM is a copy of the real dom and compares
//             and contrasts based off of state. if the state is changed,
//             virtual DOM is updated and when the virtual DOM updates,
//             the real DOM gets updated.
//         */
//         this.state = {
//             visibility: false
//         }
//     }

//     toggle() {
//         // this.setState((prevState) => {
//         //     return {
//         //         visibility: !prevState.visibility
//         //     }
//         // });
//         this.setState((prevState) => ({ visibility: !prevState.visibility }));
//     }

//     render() {
//         return (
//             <div>
//                 <h1>Small Toggle App</h1>
//                 <button onClick={this.toggle}>
//                     { this.state.visibility ? 'Hide Details' : 'Show Details' }
//                 </button>
//                 {
//                     this.state.visibility && (<p>Hey, here are the details, you can see me!</p>)
//                 }
//             </div>
//         )
//     }
// }

// class Counter extends React.Component {
//     constructor() {
//         super();
//         this.addOne = this.addOne.bind(this);
//         this.minusOne = this.minusOne.bind(this);
//         this.reset = this.reset.bind(this);
//         this.state = {
//             count: 0
//         }
//     }

//     addOne() {
//         this.setState(currentState => ({ count: currentState.count + 1 }));
//     }

//     minusOne() {
//         this.setState(currentState => ({ count: currentState.count -1 }));
//     }

//     reset() {
//         this.setState(() => ({ count: 0 }));
//     }

//     render() {
//         return (
//             <div>
//                 <h1>Count: { this.state.count }</h1>
//                 <button onClick={this.addOne}>+1</button>
//                 <button onClick={this.minusOne}>-1</button>
//                 <button onClick={this.reset}>reset</button>
//             </div>
//         )
//     }
// }

// class Header extends React.Component {
//     render() {
//         return (
//             <div>
//                 <h1>{this.props.title}</h1>
//                 <h3>{this.props.subtitle}</h3>
//             </div>
//         )
//     }
// }

/*
    PickATask
        Header
        Action
        Options
            Option
        AddOption
*/

class PickATask extends React.Component {
    constructor(props) {
        super(props);
        this.handleDeleteOption = this.handleDeleteOption.bind(this);
        this.deleteAll = this.deleteAll.bind(this);
        this.pick = this.pick.bind(this);
        this.addOptionToState = this.addOptionToState.bind(this);
        this.state = {
            options: ['Learn React!', 'Learn Angular!', 'Learn Vue!']
        }
    }

    handleDeleteOption(option) {
        this.setState(currentState => ({ options: currentState.options.filter(optionEl => option !== optionEl) }));
    }

    deleteAll() {
        this.setState({ options: [] });
    }

    pick() {
        const randomNum = Math.floor(Math.random() * this.state.options.length);
        const option = this.state.options[randomNum];
        alert(option);
    }

    addOptionToState(option) {
        if (!option) {
            return 'Enter valid value to add item!';
        } else if (this.state.options.indexOf(option) > -1) {
            return 'This option already exists!';
        }
        this.setState(prevState => ({ options: prevState.options.concat(option) }));
    }

    render() {
        return (
            <div>
                <Header title='Pick A Task' subtitle='Random Task Pick App!' />
                <Action pick={ this.pick } hasOptions={ this.state.options.length > 1 } />
                <Options deleteAll={ this.deleteAll } options={ this.state.options } handleDeleteOption={ this.handleDeleteOption } />
                <AddOption addOption={ this.addOptionToState } />
            </div>
        )
    }
}

// stateless functional component
const Header = (props) => (
    <div>
        <h1>{props.title}</h1>
        <h3>{props.subtitle}</h3>
    </div>
);

const Action = (props) => (
    <button onClick={props.pick} disabled={!props.hasOptions}>
        What should I do?
    </button>
)

const Options = (props) => (
    <div>
        { props.options.length !== 0 && <button onClick={props.deleteAll}>Remove All</button> } {/* check && as it is not treating the left as a truthy falsy.. */}
        { !props.options.length && <p>Please add an option to get started!</p> }
        {
            props.options.map((option, index) => <Option handleDeleteOption={props.handleDeleteOption} key={option + index} optionText={option} />)
        }
    </div>
)

const Option = (props) => (
    <div>
        { props.optionText }
        <button onClick={ () => { props.handleDeleteOption(props.optionText) } }>remove</button>
    </div>
)

class AddOption extends React.Component {
    constructor(props) {
        super(props);
        this.addOption = this.addOption.bind(this);
        this.state = {
            error: undefined
        }
    }

    addOption(e) {
        e.preventDefault();
        const option = e.target.elements.option.value.trim();
        const error = this.props.addOption(option);
        this.setState({ error });
        if (!error) {
            e.target.elements.option.value = '';
        }
    }

    render() {
        return (
            <div>
                { this.state.error && <p>{ this.state.error }</p> }
                <form onSubmit={this.addOption}>
                    <input type="text" name="option" />
                    <button>Add Option</button>
                </form>
            </div>
        )
    }
}

ReactDOM.render(<PickATask/>, document.getElementById('app'));