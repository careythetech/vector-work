// Testing first render example..
// class Test extends React.Component {
//     render() {
//         return (
//             <h1>Hello from React!!!??!!!</h1>
//         )
//     }
// };
// class Toggle extends React.Component {
//     constructor() {
//         super();
//         this.toggle = this.toggle.bind(this);
//         /*
//             Virtual DOM
//             virtual DOM is a copy of the real dom and compares
//             and contrasts based off of state. if the state is changed,
//             virtual DOM is updated and when the virtual DOM updates,
//             the real DOM gets updated.
//         */
//         this.state = {
//             visibility: false
//         }
//     }
//     toggle() {
//         // this.setState((prevState) => {
//         //     return {
//         //         visibility: !prevState.visibility
//         //     }
//         // });
//         this.setState((prevState) => ({ visibility: !prevState.visibility }));
//     }
//     render() {
//         return (
//             <div>
//                 <h1>Small Toggle App</h1>
//                 <button onClick={this.toggle}>
//                     { this.state.visibility ? 'Hide Details' : 'Show Details' }
//                 </button>
//                 {
//                     this.state.visibility && (<p>Hey, here are the details, you can see me!</p>)
//                 }
//             </div>
//         )
//     }
// }
// class Counter extends React.Component {
//     constructor() {
//         super();
//         this.addOne = this.addOne.bind(this);
//         this.minusOne = this.minusOne.bind(this);
//         this.reset = this.reset.bind(this);
//         this.state = {
//             count: 0
//         }
//     }
//     addOne() {
//         this.setState(currentState => ({ count: currentState.count + 1 }));
//     }
//     minusOne() {
//         this.setState(currentState => ({ count: currentState.count -1 }));
//     }
//     reset() {
//         this.setState(() => ({ count: 0 }));
//     }
//     render() {
//         return (
//             <div>
//                 <h1>Count: { this.state.count }</h1>
//                 <button onClick={this.addOne}>+1</button>
//                 <button onClick={this.minusOne}>-1</button>
//                 <button onClick={this.reset}>reset</button>
//             </div>
//         )
//     }
// }
// class Header extends React.Component {
//     render() {
//         return (
//             <div>
//                 <h1>{this.props.title}</h1>
//                 <h3>{this.props.subtitle}</h3>
//             </div>
//         )
//     }
// }

/*
    PickATask
        Header
        Action
        Options
            Option
        AddOption
*/
class PickATask extends React.Component {
  constructor(props) {
    super(props);
    this.handleDeleteOption = this.handleDeleteOption.bind(this);
    this.deleteAll = this.deleteAll.bind(this);
    this.pick = this.pick.bind(this);
    this.addOptionToState = this.addOptionToState.bind(this);
    this.state = {
      options: ['Learn React!', 'Learn Angular!', 'Learn Vue!']
    };
  }

  handleDeleteOption(option) {
    this.setState(currentState => ({
      options: currentState.options.filter(optionEl => option !== optionEl)
    }));
  }

  deleteAll() {
    this.setState({
      options: []
    });
  }

  pick() {
    const randomNum = Math.floor(Math.random() * this.state.options.length);
    const option = this.state.options[randomNum];
    alert(option);
  }

  addOptionToState(option) {
    if (!option) {
      return 'Enter valid value to add item!';
    } else if (this.state.options.indexOf(option) > -1) {
      return 'This option already exists!';
    }

    this.setState(prevState => ({
      options: prevState.options.concat(option)
    }));
  }

  render() {
    return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Header, {
      title: "Pick A Task",
      subtitle: "Random Task Pick App!"
    }), /*#__PURE__*/React.createElement(Action, {
      pick: this.pick,
      hasOptions: this.state.options.length > 1
    }), /*#__PURE__*/React.createElement(Options, {
      deleteAll: this.deleteAll,
      options: this.state.options,
      handleDeleteOption: this.handleDeleteOption
    }), /*#__PURE__*/React.createElement(AddOption, {
      addOption: this.addOptionToState
    }));
  }

} // stateless functional component


const Header = props => /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("h1", null, props.title), /*#__PURE__*/React.createElement("h3", null, props.subtitle));

const Action = props => /*#__PURE__*/React.createElement("button", {
  onClick: props.pick,
  disabled: !props.hasOptions
}, "What should I do?");

const Options = props => /*#__PURE__*/React.createElement("div", null, props.options.length !== 0 && /*#__PURE__*/React.createElement("button", {
  onClick: props.deleteAll
}, "Remove All"), " ", !props.options.length && /*#__PURE__*/React.createElement("p", null, "Please add an option to get started!"), props.options.map((option, index) => /*#__PURE__*/React.createElement(Option, {
  handleDeleteOption: props.handleDeleteOption,
  key: option + index,
  optionText: option
})));

const Option = props => /*#__PURE__*/React.createElement("div", null, props.optionText, /*#__PURE__*/React.createElement("button", {
  onClick: () => {
    props.handleDeleteOption(props.optionText);
  }
}, "remove"));

class AddOption extends React.Component {
  constructor(props) {
    super(props);
    this.addOption = this.addOption.bind(this);
    this.state = {
      error: undefined
    };
  }

  addOption(e) {
    e.preventDefault();
    const option = e.target.elements.option.value.trim();
    const error = this.props.addOption(option);
    this.setState({
      error
    });

    if (!error) {
      e.target.elements.option.value = '';
    }
  }

  render() {
    return /*#__PURE__*/React.createElement("div", null, this.state.error && /*#__PURE__*/React.createElement("p", null, this.state.error), /*#__PURE__*/React.createElement("form", {
      onSubmit: this.addOption
    }, /*#__PURE__*/React.createElement("input", {
      type: "text",
      name: "option"
    }), /*#__PURE__*/React.createElement("button", null, "Add Option")));
  }

}

ReactDOM.render( /*#__PURE__*/React.createElement(PickATask, null), document.getElementById('app'));
