const bcrypt = require('bcryptjs');

// password: password123

// bcrypt.hash('password123', 10).then(hashedPassword => {
//     console.log(hashedPassword);
//     // $2a$10$5eaORLVGHO7M2VaApZBoAuSYeWoc0rjPRjEpbwPhSK6TQFljSY5g6
//     // $2a$10$D23mLAG.6.cHAo1mWDsptuHU07ES689CjxahFGMZuCvdQubMCgk82
//     // $2a$10$3NcXxfFWlqhMVPsko8Rrj.qifwpTmP8KC0fLxTbzmWO523u.aYaoK
// });

bcrypt.compare('password123', '$2a$10$3NcXxfFWlqhMVPsko8Rrj.qifwpTmP8KC0fLxTbzmWO523u.aYaoK').then(doMatch => console.log(doMatch));