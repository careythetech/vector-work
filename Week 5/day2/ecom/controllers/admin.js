const Product = require('../models/product');
const { validationResult } = require('express-validator/check');

exports.getAddProduct = (req, res) => {
    res.render('admin/edit-product', {
        pageTitle: 'Add Product',
        path: '/admin/add-product',
        editing: false,
        errorMessage: null,
        hasError: false
    })
};

exports.postAddProduct = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).render('admin/edit-product', {
            pageTitle: 'Add Product',
            path: '/admin/edit-product',
            editing: false,
            hasError: true,
            product: {
                ...req.body
            },
            errorMessage: errors.array()[0].msg,
            validationErrors: errors.array()
        })
    }
    const image = req.file;
    const imageUrl = image.path;
    const product = new Product({ ...req.body, imageUrl });
    product.save().then(() => {
        res.redirect('/');
    }).catch(err => console.log(err));
};

exports.getEditProduct = (req, res) => {
    const editMode = req.query.edit;
    if (!editMode) {
        return res.redirect('/');
    }
    const prodId = req.params.productId;
    Product.findById(prodId).then(product => {
        if (!product) {
            return res.redirect('/');
        }
        res.render('admin/edit-product', {
            pageTitle: 'Edit Product',
            path: '/admin/edit-product',
            editing: editMode,
            product
        })
    }).catch(err => console.log(err));
}

exports.postEditProduct = (req, res) => {
    const {
        productId,
        title,
        price,
        imageUrl,
        description
    } = req.body;
    Product.findById(productId).then(product => {
        product.title = title;
        product.price = price;
        product.imageUrl = imageUrl;
        product.description = description;
        return product.save().then(() => {
            res.redirect('/admin/products');
        })
    });
}

exports.getProducts = (req, res) => {
    Product.find().then(products => {
        res.render('admin/products', {
            prods: products,
            pageTitle: 'Admin Products',
            path: '/admin/products'
        })
    }).catch(err => console.log(err));
}

exports.getProductsLocal = (req, res) => {
    console.log('hit from 3rd party..');
    Product.find().then(products => {
        res.send(products);
    }).catch(err => console.log(err));
}

exports.postDeleteProduct = (req, res) => {
    const { productId } = req.body;
    Product.deleteOne({
        _id: productId
    }).then(() => {
        res.redirect('/admin/products');
    }).catch(err => console.log(err));
}