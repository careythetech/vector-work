const fs = require('fs');
const path = require('path');
const stripe = require('stripe')('pk_test_51GqP93EHZoowScctuJ2LAPmSjO3BL3FgzkCc2MBSsfX5vPww4N9D7Jj7gJhtvtrls3H8GqPTzegJcMxzlbvmpjQA00hEhIM6WH');

const Product = require('../models/product');
const Order = require('../models/order');
const PDFDocument = require('pdfkit');

const ITEMS_PER_PAGE = 2;
exports.getIndex = (req, res) => {
    const page = +req.query.page || 1;
    let totalItems;

    Product.find()
        .countDocuments()
        .then(numProducts => {
            totalItems = numProducts;
            return Product.find().skip((page - 1) * ITEMS_PER_PAGE).limit(ITEMS_PER_PAGE)
        }).then(products => {
            res.render('shop/index', {
                prods: products,
                pageTitle: 'Shop',
                path: '/',
                currentPage: page,
                hasNextPage: ITEMS_PER_PAGE * page < totalItems,
                hasPreviousPage: page > 1,
                nextPage: page + 1,
                previousPage: page - 1,
                lastPage: Math.ceil(totalItems / ITEMS_PER_PAGE)
            })
        }).catch(err => console.log(err));
}
exports.getProducts = (req, res) => {
    const page = +req.query.page || 1;
    let totalItems;
    Product.find().countDocuments().then(numProducts => {
        totalItems = numProducts;
        return Product.find().skip((page - 1) * ITEMS_PER_PAGE).limit(ITEMS_PER_PAGE)
    }).then(prods => {
        res.render('shop/product-list', {
            prods,
            pageTitle: 'Products',
            path: '/products',
            currentPage: page,
            hasNextPage: ITEMS_PER_PAGE * page < totalItems,
            hasPreviousPage: page > 1,
            nextPage: page + 1,
            previousPage: page - 1,
            lastPage: Math.ceil(totalItems / ITEMS_PER_PAGE)
        })
    }).catch(err => console.log(err));
};

exports.getProduct = (req, res) => {
    const prodId = req.params.productId;
    Product.findById(prodId).then(product => {
        res.render('shop/product-detail', {
            product,
            pageTitle: product.title,
            path: '/products'
        })
    }).catch(err => console.log(err));
}

exports.getCart = (req, res) => {
    req.user.populate('cart.items.productId').execPopulate().then(user => {
        const products = user.cart.items;
        console.log('check products');
        console.log(products);
        res.render('shop/cart', {
            path: '/cart',
            pageTitle: 'Your Cart',
            products
        })
    })
}

exports.postCart = (req, res) => {
    const prodId = req.body.productId;
    Product.findById(prodId).then(product => {
        return req.user.addToCart(product);
    }).then(() => {
        res.redirect('/cart');
    }).catch(err => console.log(err));
}

exports.postCartDeleteProduct = (req, res) => {
    const { productId } = req.body;
    console.log('check productId..');
    console.log(productId);
    req.user.removeFromCart(productId).then(() => {
        res.redirect('/cart');
    }).catch(err => console.log(err));
}

exports.getCheckout = (req, res) => {
    req.user.populate('cart.items.productId').execPopulate().then(user => {
        const products = user.cart.items;
        let total = 0;
        products.forEach(p => {
            total += p.quantity * p.productId.price;
        });
        res.render('shop/checkout', {
            path: '/checkout',
            pageTitle: 'Checkout',
            products,
            totalSum: total
        })
    }).catch(err => console.log(err));
}

exports.postOrder = (req, res) => {
    const token = req.body.stripeToken;
    let totalSum = 0;

    req.user.populate('cart.items.productId').execPopulate()
        .then(user => {
            console.log('first then..');
            user.cart.items.forEach(p => {
                totalSum += p.quantity * p.productId.price;
            });

            const products = user.cart.items.map(i => ({ quantity: i.quantity, product: { ...i.productId._doc } }));

            const order = new Order({
                user: {
                    email: req.user.email,
                    userId: req.user
                },
                products
            });
            return order.save();
        }).then(result => {
            console.log('2nd then..');
            // stripe charge..
            const charge = stripe.charges.create({
                amount: totalSum * 100,
                currency: 'usd',
                description: 'Demo Order',
                source: token,
                metadata: { order_id: result._id.toString() }
            });
            return req.user.clearCart();
        }).then(() => {
            console.log('3rd then');
            res.redirect('/orders');
        }).catch(err => console.log(err));
}

exports.getOrders = (req, res) => {
    Order.find({ 'user.userId': req.user._id })
        .then(orders => {
            res.render('shop/orders', {
                path: '/orders',
                pageTitle: 'Your Orders',
                orders
            })
        })
}

exports.getInvoice = (req, res, next) => {
    const orderId = req.params.orderId;
    Order.findById(orderId)
        .then(order => {
            if (!order) {
                return next(new Error('order not found!!!'));
            }
            if (order.user.userId.toString() !== req.user._id.toString()) {
                return next(new Error('unauthorized!'));
            }
            const invoiceName = `invoice-${orderId}.pdf`;
            const invoicePath = path.join('data', 'invoices', invoiceName);
            const pdfDoc = new PDFDocument();
            res.setHeader('Content-Type', 'application/pdf');
            res.setHeader('Content-Disposition', `inline; filename="${invoiceName}"`);
            pdfDoc.pipe(fs.createWriteStream(invoicePath));
            pdfDoc.pipe(res);

            pdfDoc.fontSize(26).text('Invoice', { underline: true });

            pdfDoc.text('--------------------');
            let totalPrice = 0;
            order.products.forEach(prod => {
                totalPrice += prod.quantity * prod.product.price;
                pdfDoc
                    .fontSize(14)
                    .text(`${prod.product.title} - ${prod.quantity} x $${prod.product.price}`);
            });
            pdfDoc.text('--------------------');
            pdfDoc.fontSize(20).text(`Total Price: $${totalPrice}`);
            pdfDoc.end();
        }).catch(err => console.log(err));
}