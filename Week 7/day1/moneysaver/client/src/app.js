import React from 'react';
import ReactDOM from 'react-dom';
import AppRouter from './routers/AppRouter';
import configureStore from './store/configureStore';
import { Provider } from 'react-redux'
import 'normalize.css/normalize.css';
import './styles/styles.scss';
import { startSetExpenses } from './actions/expenses';

const store = configureStore();

store.subscribe(() => {
    console.log('checking current state..');
    console.log(store.getState());
});

const jsx = (
    <Provider store={store}>
        <AppRouter />
    </Provider>
);

const renderApp = () => {
    ReactDOM.render(jsx, document.getElementById('app'));
};

store.dispatch(startSetExpenses()).then(() => {
    renderApp();
});