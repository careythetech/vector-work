import React from 'react';
import { NavLink, Link } from 'react-router-dom';

const Header = () => (
    <header className="header">
        <div className="content-container">
            <div className="header__content">
                <Link className="header__title" to="/">
                    <h1>Money Saver</h1>
                </Link>
                <NavLink to="/help" activeClassName="is-active" exact={true}>
                    <span style={{ color: "white" }}>Help</span>
                </NavLink>
            </div>
        </div>
    </header>
);

export default Header;