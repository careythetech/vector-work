import React from 'react';
import ExpenseForm from './ExpenseForm';
import { connect } from 'react-redux';
import { removeExpense, startEditExpense } from '../actions/expenses';

const EditExpensePage = (props) => (
    <div>
        <div className="page-header">
            <div className="content-container">
                <h1 className="page-header__title">Edit Expense</h1>
            </div>
        </div>
        <div className="content-container">
            <ExpenseForm
                expense={props.expense}
                editMode={true}
                onSubmit={(expense) => {
                    props.dispatch(startEditExpense(props.expense._id, expense));
                    props.history.push('/');
                }}
            />
            <button onClick={() => {
                props.dispatch(removeExpense({ id: props.expense.id }));
                props.history.push('/');
            }}className="button button--secondary">Remove</button>
        </div>
    </div>
);

const mapStateToProps = (state, props) => {
    console.log(state.expenses);
    return {
        expense: state.expenses.find(expense => expense._id === props.match.params.id)
    }
}

export default connect(mapStateToProps)(EditExpensePage);