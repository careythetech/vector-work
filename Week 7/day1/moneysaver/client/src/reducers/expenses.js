const expensesReducerDefaultState = [];

export default (state = expensesReducerDefaultState, action) => {
    switch (action.type) {
        case 'ADD_EXPENSE':
            console.log('Step 6: expense received, updating store');
            console.log(action.expense);
            return [ ...state, action.expense ];
        case 'REMOVE_EXPENSE':
            return state.filter(({ id }) => id !== action.id);
        case 'EDIT_EXPENSE':
            return state.map(expense => {
                if (expense._id === action._id) {
                    return {
                        ...expense,
                        ...action.updates
                    }
                } else {
                    return expense;
                };
            })
        case 'SET_EXPENSES':
            return action.expenses;
        default:
            return state;
    }
}