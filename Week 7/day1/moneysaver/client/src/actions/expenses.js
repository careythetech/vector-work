export const addExpense = (expense) => {
    console.log('Step 4: payload received from action function');
    console.log(expense);
    console.log('Step 5: lets update the store!');
    return {
        type: 'ADD_EXPENSE',
        expense
    }
}

export const startAddExpense = (expenseData = {}) => {
    console.log('Step 2: Payload received, lets call to the database');
    return async (dispatch, getState) => {
        const body = JSON.stringify({ ...expenseData });
        const req = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body
        };
        const result = await fetch('http://localhost:3000/add-expense', req);
        const savedExpense = await result.json();
        console.log('Step 3: result from database, lets give it to addExpense action');
        console.log(savedExpense);
        dispatch(addExpense({ ...savedExpense }));
    }
}

export const setExpenses = (expenses) => ({ type: 'SET_EXPENSES', expenses });

export const startSetExpenses = () => {
    return async (dispatch) => {
        const result = await fetch('http://localhost:3000/expenses');
        const data = await result.json();
        console.log(data);
        dispatch(setExpenses(data));
    }
}

export const removeExpense = ({ id } = {}) => ({ type: 'REMOVE_EXPENSE', id });



export const editExpense = (_id, updates) => ({ type: 'EDIT_EXPENSE', _id, updates });

export const startEditExpense = (id, updates) => {
    return async (dispatch) => {
        const body = JSON.stringify({ _id: id, ...updates });
        const req = {
            method: 'POST', // change to put??
            headers: {
                'Content-Type': 'application/json'
            },
            body
        };
        const result = await fetch('http://localhost:3000/edit-expense', req);
        const data = await result.json();
        const { _id, note, description, amount, createdAt } = data;
        dispatch(editExpense(_id, { note, description, amount, createdAt }));
    }
}