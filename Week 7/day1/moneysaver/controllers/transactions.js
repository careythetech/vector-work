const Expense = require('../models/expense');

exports.addExpense = (req, res) => {
    const expense = new Expense({ ...req.body });
    expense.save().then((exp) => {
        const { _id, description, amount, createdAt, note } = exp;
        res.send({ id: _id, description, amount, createdAt, note });
    }).catch(err => console.log(err));
};

exports.getExpenses = (req, res) => {
    Expense.find().then((expenses) => {
        res.send(expenses)
    }).catch(err => console.log(err));
};

exports.postEditExpense = (req, res) => {
    const { _id, note, description, createdAt, amount } = req.body;
    console.log('check variables..');
    console.log(req.body);
    Expense.findById(_id).then(expense => {
        expense.note = note;
        expense.description = description;
        expense.amount = amount;
        expense.createdAt = createdAt;
        return expense.save().then((updatedExpense) => {
            res.send(updatedExpense);
        });
    })
}