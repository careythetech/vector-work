const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

const expensesRoutes = require('./routes/transactions');

const MONGODB_URI = 'mongodb://localhost:27017/moneysaver';

const app = express();

app.use(bodyParser.json());
app.use(cors());
app.use(expensesRoutes);

mongoose.connect(MONGODB_URI).then(() => {
    console.log('connected to mongodb using mongoose!');
    app.listen(3000);
}).catch(err => console.log(err));