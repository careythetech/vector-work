const bcryptjs = require('bcryptjs');

const User = require('../models/user');

exports.login = async (req, res) => {
    const { body: { email, password } } = req;

    const user = await User.findOne({ email });
    if (!user) {
        return res.send({
            status: 422,
            message: 'User not found'
        })
    }

    const thereIsMatch = await bcryptjs.compare(password, user.password);
    if (thereIsMatch) {
        // generate a token..
        return res.send({
            status: 200,
            message: 'Logged in!',
        });
    } else {
        return res.send({
            status: 422,
            message: 'Username / Password do not match.',
        });
    }
}

exports.signUp = async (req, res) => {
    const { body: { email, password } } = req;
    const hashedPassword = await bcryptjs.hash(password, 12);
    const user = new User({
        email,
        password: hashedPassword,
        expenses: []
    });
    await user.save();
    res.send({
        status: 200,
        message: 'User Created!'
    });
}