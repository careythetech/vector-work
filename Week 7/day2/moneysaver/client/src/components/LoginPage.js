import React from 'react';
// import { connect } from 'react-redux';
import { startLogin } from '../actions/auth';

class LoginPage extends React.Component {
    state = {
        email: '',
        password: ''
    }

    onEmailChange = (e) => {
        const email = e.target.value;
        this.setState({ email });
    }

    onPasswordChange = (e) => {
        const password = e.target.value;
        this.setState({ password });
    }

    signup = async () => {
        const req = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state)
        }
        const result = await fetch('http://localhost:3000/signup', req);
        const data = await result.json();
        if (data.status === 200) {
            alert('user created!');
            this.setState({ email: '', password: '' })
        }
    }
    render() {
        return (
            <div className="box-layout">
                <div className="box-layout__box">
                    <h1 className="box-layout__title">Money Saver</h1>
                    <p>Got Money?</p>
                    <button className="button" onClick={startLogin(this.state)}>
                        Login
                    </button>
                    <button className="button" onClick={this.signup}>
                        Signup
                    </button>
                    <input
                        type="email"
                        placeholder="Email"
                        autoFocus
                        className="text-input"
                        value={this.state.email}
                        onChange={this.onEmailChange}
                    />
                    <input
                        type="password"
                        className="text-input"
                        placeholder="Password"
                        value={this.state.password}
                        onChange={this.onPasswordChange}
                    />
                </div>
            </div>
        )
    }
}

// const mapDispatchToProps = (dispatch) => ({ startLogin: (user) => {
//     return dispatch(startLogin(user));
// }});

export default LoginPage