export default (state = { isLoggedIn: false }, action) => {
    switch (action.type) {
        case 'LOGINLOGOUT':
            return {
                isLoggedIn: action.bool
            }
        default:
            return state;
    }
}