export const loginLogout = (bool) => {
    return {
        type: 'LOGINLOGOUT',
        bool
    };
}

export const startLogin = (user) => {
    return async (dispatch) => {
        const req = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user)
        }
        console.log('hit??');
        const result = await fetch('http://localhost:3000/login', req);
        const data = await result.json();
        const { status } = data;
        if (status === 200) {
            console.log(dispatch);
            dispatch(loginLogout(true));
        }
    };
    return async (dispatch) => {
        const body = JSON.stringify({ _id });
        const req = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body
        }
        const result = await fetch('http://localhost:3000/delete-expense', req);
        console.log(result);
        dispatch(removeExpense({ _id }));
    }
};



// export const startSignup = (user) => {
//     return async ()
// }

export const startLogout = () => {
    return (dispatch) => {
        setTimeout(() => {
            dispatch(loginLogout(false));
        }, 1000);
    }
};