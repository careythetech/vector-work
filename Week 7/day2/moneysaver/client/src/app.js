import React from 'react';
import ReactDOM from 'react-dom';
import AppRouter from './routers/AppRouter';
import configureStore from './store/configureStore';
import { Provider } from 'react-redux'
import 'normalize.css/normalize.css';
import './styles/styles.scss';
import { startSetExpenses } from './actions/expenses';
import { startLogin } from './actions/auth';
import LoadingPage from './components/LoadingPage';

const store = configureStore();
let appRendered = false;

const jsx = (
    <Provider store={store}>
        <AppRouter />
    </Provider>
);

const renderApp = () => {
    ReactDOM.render(jsx, document.getElementById('app'));
};

ReactDOM.render(<LoadingPage />, document.getElementById('app'));

store.subscribe(() => {
    const { auth: { isLoggedIn } } = store.getState();
    console.log('state changed.. check if they are logged in');
    console.log(isLoggedIn);
    // if (!appRendered) {
    //     if (isLoggedIn) {
    //         store.dispatch(startSetExpenses()).then(() => {
    //             appRendered = true;
    //             console.log('testing only hit once?');
    //             renderApp();
    //         });
    //     } else {
    //         appRendered = true;
    //         renderApp();
    //     }
    // }
});

setTimeout(() => {
    renderApp();
}, 500);
