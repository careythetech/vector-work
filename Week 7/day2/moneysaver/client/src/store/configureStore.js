import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import expenses from '../reducers/expenses';
import filters from '../reducers/filters';
import auth from '../reducers/auth';

export default () => {
    const store = createStore(
        combineReducers({ expenses, filters, auth }),
        compose(applyMiddleware(thunk))
    );
    return store;
};