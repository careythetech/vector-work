const express = require('express');

const transactionController = require('../controllers/transactions');

const router = express.Router();

const { addExpense, getExpenses, postEditExpense, postDeleteExpense } = transactionController;

router.post('/add-expense', addExpense);
router.get('/expenses', getExpenses);
router.post('/edit-expense', postEditExpense);
router.post('/delete-expense', postDeleteExpense);

module.exports = router;