const express = require('express');

const userAuthController = require('../controllers/userAuth');

const router = express.Router();

const { login, signUp } = userAuthController;

router.post('/login', login);
router.post('/signup', signUp);

module.exports = router;