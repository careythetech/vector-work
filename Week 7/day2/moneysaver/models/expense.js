const mongoose = require('mongoose');
const { Schema } = mongoose;

const expenseSchema = new Schema({
    description: {
        type: String,
        required: true
    },
    amount: {
        type: Number,
        required: true
    },
    note: {
        type: String,
        required: false
    },
    createdAt: {
        type: Date,
        required: true
    }
});

module.exports = mongoose.model('Expense', expenseSchema);