const mongoose = require('mongoose');

const { Schema } = mongoose;

const userSchema = new Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    expenses: []
    // expenses: [
    //     {
    //         expenseId: {
    //             type: Schema.Types.ObjectId,
    //             ref: 'Expense',
    //             required: true
    //         }
    //     }
    // ]
});

module.exports = mongoose.model('User', userSchema);