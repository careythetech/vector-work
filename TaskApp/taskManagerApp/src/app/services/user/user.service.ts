import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


const api_url = 'http://localhost:8080/api';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor( private http: HttpClient) { }

  getUsersRequest(): Observable<any> {
    return this.http.get(api_url + '/getuser?title=', httpOptions);
  }
  getUserById(id): Observable<any> {
    return this.http.get(api_url + `/getUser/${id}`, httpOptions);
  }

  
}
