import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { Observable } from 'rxjs';


const auth_api = "http://localhost:8080/auth/";

const httpOptions: {headers: HttpHeaders} = {
  headers: new HttpHeaders({ "Content-Type": "application/json" })
}
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  constructor(private http: HttpClient,) { }

  signIn(credentials): Observable<any>{
    return this.http.post(auth_api + 'signIn', {
      username: credentials.username,
      password: credentials.password
    }, httpOptions)

  }

  signUp(user): Observable<any>{
    return this.http.post(auth_api + 'signUp', {
      avatar: user.avatar,
      firstname: user.firstname,
      lastname: user.lastname,
      email: user.email,
      password: user.password
    }, httpOptions)
  }
}
