import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserService } from '../user/user.service'


const api_url = "http://localhost:8008/api/groups"; 

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};
@Injectable({
  providedIn: 'root'
})
export class GroupService {

  constructor( private userService: UserService, private http: HttpClient) { }

  createGroupRequest(): Observable<any> {
    return this.http.post(api_url + '/createGroup/', httpOptions);
  }
  getGroupRequest(): Observable<any> {
    return this.http.get(api_url + '/getAllGroups/', httpOptions);
  }
  // getGroupId(): Observable<any> {
  //   return this.http.get(api_url + `/getGroupById/${id}`, httpOptions);
  // }

}
