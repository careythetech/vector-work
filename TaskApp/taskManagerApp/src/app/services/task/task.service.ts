import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';



const api_url = 'http://localhost:8080/api/tasks';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
}
@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private http: HttpClient) { }

  getTask(id): Observable<any> {
    return this.http.get(api_url + `/getTaskById/${id}`, httpOptions);
  }

  createTask(task:any): Observable<any> {
    return this.http.post(api_url + '/createTask', httpOptions);
  }


}
