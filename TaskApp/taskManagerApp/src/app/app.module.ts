import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from "@angular/router";
import { FormsModule } from '@angular/forms';



import { SignupComponent } from './components/signup/signup.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';



import { ProfileComponent } from './components/profile/profile.component';
import { EditTaskComponent } from './components/edit-task/edit-task.component';
import { GroupDetailsComponent } from './components/group-details/group-details.component';
import { GroupsComponent } from './components/groups/groups.component';
import { NewInvitationsComponent } from './components/new-invitations/new-invitations.component';
import { MenuComponent } from './components/menu/menu.component';
import { NewRequestsComponent } from './components/new-requests/new-requests.component';
import { NewUsersComponent } from './components/new-users/new-users.component';
import { NewGroupComponent } from './components/new-group/new-group.component';
import { NewTaskComponent } from './components/new-task/new-task.component';
import { SigninComponent } from './components/signin/signin.component';


@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    HomeComponent,
    NavbarComponent,
    ProfileComponent,
    EditTaskComponent,
    GroupDetailsComponent,
    GroupsComponent,
    NewInvitationsComponent,
    MenuComponent,
    NewRequestsComponent,
    NewUsersComponent,
    NewGroupComponent,
    NewTaskComponent,
    SigninComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule,
    FormsModule
  ],
  providers: [
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
