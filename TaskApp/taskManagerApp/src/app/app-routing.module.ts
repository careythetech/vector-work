import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SignupComponent } from './components/signup/signup.component';
import { SigninComponent } from './components/signin/signin.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule} from '@angular/forms';


import { HomeComponent } from './components/home/home.component';


const routes: Routes = [
  { path: " ", component: HomeComponent},
  { path: "signin", component: SigninComponent},
  { path: "signup", component: SignupComponent},
  { path: "**", redirectTo: ""},
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
