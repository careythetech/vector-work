import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewInvitationsComponent } from './new-invitations.component';

describe('NewInvitationsComponent', () => {
  let component: NewInvitationsComponent;
  let fixture: ComponentFixture<NewInvitationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewInvitationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewInvitationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
