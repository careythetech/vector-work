import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { AuthService } from "src/app/services/auth/auth.service";

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.scss"],
})
export class SignupComponent implements OnInit {
  signUpForm: FormGroup;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.signUpForm = this.createFormGroup();
  }

  createFormGroup(): FormGroup {
    return new FormGroup({
      avatar: new FormControl("", [Validators.required, Validators.minLength(8)]),
      firstname: new FormControl("", [Validators.required, Validators.minLength(8)]),
      lastname: new FormControl("",[Validators.required, Validators.minLength(8)]),
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [Validators.required, Validators.minLength(8),
      ]),
    });
  }

  signUp(): void {
    this.authService.signUp(this.signUpForm.value).subscribe((msg) => {
      console.log(msg);
      this.router.navigate(["login"]);
    });
    console.log("Signup")

  }
}