import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from "@angular/router";

import { AuthService } from "src/app/services/auth/auth.service";

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  signInForm: FormGroup

  ngOnInit(): void {
    this.signInForm = this.createSigninForm()
  }
  createSigninForm(): FormGroup {
    return new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(7)]),
    
    })
  }

  signIn(): void {
    this.authService.signIn(this.signInForm.value).subscribe((msg) => {
      console.log(msg);
      this.router.navigate(["profile"]);
    });
  }
}

