const db = require('../util/database');


// User model
module.exports = class User {
    constructor(name, email,password){
        this.name = name,
        this.email = email,
        this.password = password
    }


    static find(email){
        return db.excute(
            'SELECT * FROM users WHERE email = ?', [email]
        )
    }
    static save(user){
        return db.excute(
            'INSERT INTO users (name, email, password) VALUES (?, ?, ?)', 
            [user.name, user.email,user.password]
        )
    }
};

