import React from 'react';
import { NavLink, Link } from 'react-router-dom';

const Header = () => (
    <header className="header">
        <div className="content-container">
            <div className="header__content">
                <Link className="header__title" to="/dashboard">
                    <h1>Money Saver</h1>
                </Link>
                <NavLink to="/" activeClassName="is-active" exact={true}>
                    <span style={{ color: "white" }}>Dashboard</span>
                </NavLink>
                <NavLink to="/create" activeClassName="is-active" exact={true}>
                    <span style={{ color: "white" }}>Create Expense</span>
                </NavLink>
                <NavLink to="/help" activeClassName="is-active" exact={true}>
                    <span style={{ color: "white" }}>Help</span>
                </NavLink>
            </div>
        </div>
    </header>
);

export default Header;