import React from 'react';
import Header from '../components/Header';
import HomePage from '../components/HomePage';
import ContactPage from '../components/ContactPage';
import PortfolioItemPage from '../components/PortfolioItemPage';
import PortfolioPage from '../components/PortfolioPage';
import NotFoundPage from '../components/NotFoundPage';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

const AppRouter = () => (
    <BrowserRouter>
        <div>
            <Header />
            <Switch>
                <Route exact path="/" component={HomePage} />
                <Route exact path="/portfolio" component={PortfolioPage} />
                <Route exact path="/portfolio/:id" component={PortfolioItemPage} />
                <Route exact path="/contact" component={ContactPage} />
                <Route component={NotFoundPage} />
                {/* 
                Another way to render routes..
                <Route exact path="/">
                    <HomePage />
                </Route>
                <Route exact path="/portfolio">
                    <PortfolioPage />
                </Route> */}
            </Switch>
        </div>
    </BrowserRouter>
);

export default AppRouter;