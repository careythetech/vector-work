import React from 'react';
import { Link } from 'react-router-dom';

const PortfolioPage = () => (
    <div>
        <h1>My Goals</h1>
        <p>info</p>
        <Link to="/portfolio/seniordev">Senior Dev</Link>
        <Link to="/portfolio/makepie">Make Pie</Link>
    </div>
);

export default PortfolioPage;