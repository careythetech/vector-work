import React from 'react';

const ContactPage = () => (
    <div>
        <h1>Contact Me</h1>
        <p>You can react me at joshua.lerma@vectornow.com</p>
    </div>
);

export default ContactPage;