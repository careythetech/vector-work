import React from 'react';
import ExpenseListItem from './ExpenseListItem';
import { connect } from 'react-redux';

const ExpenseList = ({ expenses }) => (
    <div>
        <h1>Expense List</h1>
        {
            expenses.map(expense => <ExpenseListItem key={expense.id} { ...expense } />)
        }
    </div>
);

const mapStateToProps = (state) => {
    return {
        expenses: state
    }
}

export default connect(mapStateToProps)(ExpenseList);