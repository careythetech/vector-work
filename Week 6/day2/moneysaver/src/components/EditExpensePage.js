import React from 'react';
import ExpenseForm from './ExpenseForm';
import { connect } from 'react-redux';

const EditExpensePage = (props) => (
    <div>
        <ExpenseForm
            expense={props.expense}
            editMode={true}
        />
        <button>Remove</button>
    </div>
);

const mapStateToProps = (state, props) => {
    return {
        expense: state.find(expense => expense.id === props.match.params.id)
    }
}

export default connect(mapStateToProps)(EditExpensePage);