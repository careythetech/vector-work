import React from 'react';

import Header from './Header';
import Action from './Action';
import AddOption from './AddOption';
import Options from './Options';
import OptionModal from './OptionModal';

export default class PickATask extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            options: [],
            selectedOption: undefined
        }
    }

    componentDidMount() {
        const json = localStorage.getItem('options');
        const options = JSON.parse(json);
        if (options) {
            this.setState({ options })
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.options.length !== this.state.options.length) {
            const json = JSON.stringify(this.state.options);
            localStorage.setItem('options', json);
        }
    }

    handleDeleteOption = (option) => {
        this.setState(currentState => ({ options: currentState.options.filter(optionEl => option !== optionEl) }));
    }

    deleteAll = () => {
        this.setState({ options: [] });
    }

    handleClearSelectedOption = () => {
        this.setState({ selectedOption: undefined });
    }

    pick = () => {
        const randomNum = Math.floor(Math.random() * this.state.options.length);
        const selectedOption = this.state.options[randomNum];
        this.setState({ selectedOption });
    }

    addOptionToState = (option) => {
        if (!option) {
            return 'Enter valid value to add item!';
        } else if (this.state.options.indexOf(option) > -1) {
            return 'This option already exists!';
        }
        this.setState(prevState => ({ options: prevState.options.concat(option) }));
    }

    render() {
        return (
            <div>
                <Header title='Pick A Task' subtitle='Random Task Pick App!' />
                <div className="container">
                    <Action pick={ this.pick } hasOptions={ this.state.options.length > 1 } />
                    <div className="widget">
                        <Options deleteAll={ this.deleteAll } options={ this.state.options } handleDeleteOption={ this.handleDeleteOption } />
                        <AddOption addOption={ this.addOptionToState } />
                    </div>
                </div>
                <OptionModal
                    selectedOption={this.state.selectedOption}
                    handleClearSelectedOption={this.handleClearSelectedOption}
                />
            </div>
        )
    }
}