import React from 'react';
import Option from './Option';

const Options = (props) => (
    <div>
        <div className="widget-header">
            <h3 className="widget-header__title">Your Options</h3>
            { props.options.length !== 0 && <button className="button button--link" onClick={props.deleteAll}>Remove All</button> } {/* check && as it is not treating the left as a truthy falsy.. */}
        </div>
        { !props.options.length && <p className="widget__message">Please add an option to get started!</p> }
        {
            props.options.map((option, index) => <Option handleDeleteOption={props.handleDeleteOption} key={option + index} optionText={option} />)
        }
    </div>
);

export default Options;