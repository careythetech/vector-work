import React from 'react';
import ReactDOM from 'react-dom';
import PickATask from './PickATask';

import 'normalize.css/normalize.css';
import './styles/styles.scss';

/*
    REDUX 101

    store - where the data is placed for components to communicate with
    reducer - it awaits actions to be fed to the reducer and based on 
              the action type, perform state change. actions dispatched
              is the only way to trigger the reducer which ultimatley
              updates the state.
    actions - these are objects that contain information on how to update
              the store.
    dispatching - this takes actions as an arguement and once triggered, will
                  let the reducer know what to update in the store.
*/

import { createStore } from 'redux';

// Action generators - functions that return action objects
const incrementCount = ({ incrementBy = 1 } = {}) => ({ type: 'INCREMENT', incrementBy})
const decrementCount = ({ decrementBy = 1 } = {}) => ({ type: 'DECREMENT', decrementBy})
const resetCount = () => ({ type: 'RESET' });
const setCount = ({ count }) => ({ type: 'SET', count });

// reducer
const countReducer = (state = { count: 0 }, action) => {
    switch (action.type) {
        case 'INCREMENT':
            return {
                count: state.count + action.incrementBy
            };
        case 'DECREMENT':
            return {
                count: state.count - action.decrementBy
            };
        case 'RESET':
            return {
                count: 0
            };
        case 'SET':
            return {
                count: action.count
            };
        default:
            return state;
    }
}

const store = createStore(countReducer);

store.subscribe(() => {
    console.log(`%cCheck state after dispatch: ` + JSON.stringify(store.getState()), 'font-size: 24px; color: black; background: white;');
});

store.dispatch(incrementCount({ incrementBy: 5 }));
store.dispatch(incrementCount({ incrementBy: 2 }));
store.dispatch(incrementCount());
store.dispatch(decrementCount());
store.dispatch(decrementCount({ decrementBy: 5 }));
store.dispatch(resetCount());
store.dispatch(setCount({ count: 100 }));

ReactDOM.render(<PickATask/>, document.getElementById('app'));